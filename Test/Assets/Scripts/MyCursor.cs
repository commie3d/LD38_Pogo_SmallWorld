﻿using UnityEngine;
using System.Collections;

public class MyCursor : MonoBehaviour
{
    CursorLockMode wantedMode = CursorLockMode.Locked;

    // Apply requested cursor state
    void SetCursorState()
    {
        Cursor.lockState = wantedMode;
        // Hide cursor when locking
        Cursor.visible = (CursorLockMode.Locked != Cursor.lockState);
    }

    void Update()
    {
        // Release cursor on escape keypress
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            switch (Cursor.lockState)
            {
                case CursorLockMode.None:
                    wantedMode = CursorLockMode.Locked;
                    break;
                case CursorLockMode.Confined:
                    wantedMode = CursorLockMode.None;
                    break;
                case CursorLockMode.Locked:
                    wantedMode = CursorLockMode.None;
                    break;
            }
        }

        SetCursorState();
    }
}