﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Button makes a dot shape when pushed (only the light under the button)
 * |   |
 * | O |
 * |   |
 * 
 */
public class PuzzleButtonDot : PuzzleButton
{
    public PuzzleButtonDot(int _x, int _y, PuzzleGrid _grid) : base(_x, _y, _grid)
    {
    }

    protected override void getAffected()
    {
        // . shape
        PuzzleBit bit;
        bit = grid.getBitAt(x, y); // center
        if (bit != null) affectedBits.Add(bit);
    }
}