﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Button makes a dot shape when pushed (only the light under the button)
 * |   |
 * | O |
 * |   |
 * 
 */
public class PuzzleButtonFake : PuzzleButton
{
    public PuzzleButtonFake(int _x, int _y, PuzzleGrid _grid) : base(_x, _y, _grid)
    {
    }

    protected override void getAffected()
    {
        // no bits affected by fake button
    }
}