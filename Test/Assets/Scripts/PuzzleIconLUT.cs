﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleIconLUT : Singleton<PuzzleIconLUT>
{
    [Serializable]
    public struct CharTransform
    {
        public char c;
        public Transform t;
    }
    public CharTransform[] cts;

    public Dictionary<char, Transform> icons;

    protected PuzzleIconLUT() // guarantee this will be always a singleton only - can't use the constructor!
    {} 

    void Awake()
    {
        icons = new Dictionary<char, Transform>();
        foreach (CharTransform ct in cts)
        {
            icons.Add(ct.c, ct.t);
        }
    }
}