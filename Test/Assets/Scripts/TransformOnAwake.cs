﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformOnAwake: MonoBehaviour {

    public Transform newTransform;

    private void Awake()
    {
        if (newTransform != null)
        {
            transform.position = newTransform.position;
            transform.localScale = newTransform.localScale;
        }
    }
}