﻿using UnityEngine;
using System.Collections;
 
[ExecuteInEditMode]
public class Portal : MonoBehaviour
{
	private bool m_DisablePixelLights = true;
    public Color rimColor = Color.magenta;
 
	public  LayerMask m_ReflectLayers = -1;
	private Hashtable m_ReflectionCameras = new Hashtable(); // Camera -> Camera table
	private RenderTexture _portalTex;
    public  GameObject portalExit;
 
	private static bool s_InsideRendering = false;
    public float portalNearClip = 0.1f;

    private Collider radiusTrigger;

    public GameObject exitMarker;

    private Vector3 trackedPos;
    private Vector3 playerPos;

    BoxCollider portalCollider;

    private bool justPorted = false;

    private float lastPosSign;

    private Camera portalCamera;

    public void Start()
    {
        radiusTrigger = GetComponent<SphereCollider>();

        if (exitMarker)
        {
            exitMarker.transform.localScale = Vector3.one;
            exitMarker.GetComponent<Renderer>().material.SetColor("_Color", Color.green);
            exitMarker.transform.localScale = exitMarker.transform.localScale * ScaleFactor * 0.35f;
        }

        trackedPos = Vector3.zero;
        portalCollider = GetComponent<BoxCollider>();

        Debug.Log(ScaleFactor.ToString());
        Debug.Log(PortalsOffset.ToString());
    }

    private float ScaleFactor
    {
        get
        {
            if (!portalExit)
                return 1f;

            Renderer rEnt = GetComponent<Renderer>();
            Renderer rExt = portalExit.GetComponent<Renderer>();

            float _sizeEnt = rEnt.bounds.size.magnitude;
            float _sizeExt = rExt.bounds.size.magnitude;

            return _sizeExt / _sizeEnt;
        }
    }

    // Ent and Ext portals are >> RELATIVE << designations
    // Entrance portal center
    private Vector2 EntPortalCenter
    {
        get
        {
            var center = GetComponent<Renderer>().bounds.center;
            return new Vector2(center.x, center.z);
        }
    }

    // Exit portal center
    private Vector2 ExtPortalCenter
    {
        get
        {
            var center = portalExit.GetComponent<Renderer>().bounds.center;
            return new Vector2(center.x, center.z);
        }
    }

    private Vector2 PortalsOffset
    {
        get { return EntPortalCenter - ExtPortalCenter; }
    }

    // Track player while surrounding portal
    private void OnTriggerStay(Collider player)
    {
        if (player.tag != "Player")
            return;

        Bounds portalBounds = GetComponent<Renderer>().bounds;

        /////////////////////////////////////////////////////////////////////////

        // Track player within current portal's trigger radius
        Vector2 playerPos = new Vector2(player.transform.position.x, player.transform.position.z);

        // Store offset from portal
        Vector2 offset = playerPos - EntPortalCenter;

        // Scale offset by difference in portal scales 
        Vector2 scaledPos = EntPortalCenter + offset * ScaleFactor;

        // Finally, offset to the exits position
        scaledPos += -PortalsOffset;

        trackedPos = new Vector3(scaledPos.x, portalExit.GetComponent<Renderer>().bounds.center.y, scaledPos.y);

        /////////////////////////////////////////////////////////////////////////

        Vector3 pFwd = portalBounds.center + portalCollider.transform.forward;

        //Debug.Log(relPlayerPos.ToString());

        Debug.DrawLine(portalBounds.center, pFwd, Color.magenta);

        Vector3 playerWorldPos = player.gameObject.transform.position;
        Vector3 relPlayerPos = portalCollider.transform.InverseTransformPoint(playerWorldPos);

        Physics.IgnoreCollision( player.GetComponent<Collider>(), portalCollider);

        PlayerMovement _pm = player.gameObject.GetComponent<PlayerMovement>();

        if(portalCamera)
            portalCamera.transform.position = trackedPos;

        if (relPlayerPos.z < 15)
        {
            transform.localScale *= -1;
            portalExit.transform.localScale *= -1;
        }

        if (portalCollider.bounds.Contains(player.bounds.center + player.transform.forward.normalized * 0.01f )) //+ _pm.Direction * 0.02f ) )
        {
            //if (relPlayerPos.z < 0)
            //{
            //    transform.localScale *= -1f;
            //    //portalCollider.transform.localScale *= -1f;
            //    //portalExit.transform.localScale *= -1f;
            //    //justPorted = true;
            //    return;
            //}

            _pm.RunSpeed *= ScaleFactor;
            _pm.WalkSpeed *= ScaleFactor;
            player.transform.localScale = player.transform.localScale * ScaleFactor;

            //player.gameObject.GetComponent<PlayerMovement>().JumpForce *= ScaleFactor;

            player.transform.position = trackedPos; // player.transform.forward * 0.02f;

            player.transform.position += _pm.Direction.normalized * player.bounds.extents.z / 4;// _pm.Direction * player.bounds.extents.z / 2;
            touchGround(player.transform);

            if (relPlayerPos.z < 0)
            {
                //transform.localScale *= -1f;
                //portalCollider.transform.localScale *= -1f;
                //portalExit.transform.localScale *= -1f;
                //justPorted = true;
                return;
            }
        }

        if(exitMarker)
            exitMarker.transform.position = trackedPos;
    }

    // This is called when it's known that the object will be rendered by some
    // camera. We render reflections and do other updates here.
    // Because the script executes in edit mode, reflections for the scene view
    // camera will just work!
    public void OnWillRenderObject()
	{
		var rend = GetComponent<Renderer>();

        if (!enabled || !rend || !rend.sharedMaterial || !rend.enabled)
			return;
 
		Camera cam = Camera.current;

        var _mat = rend.material;

        if (_mat.HasProperty("_RimColor"))
            _mat.SetColor("_RimColor", rimColor);

        if ( !cam )
			return;

        if( !portalExit )
            return;
 
		// Safeguard from recursive reflections.        
		if( s_InsideRendering )
            return;

		s_InsideRendering = true;
 
		//Camera portalCamera;
		CreateMirrorObjects( cam, out portalCamera );
 
		// Optionally disable pixel lights for reflection
		int oldPixelLightCount = QualitySettings.pixelLightCount;
		if( m_DisablePixelLights )
			QualitySettings.pixelLightCount = 0;

        portalCamera.projectionMatrix = cam.projectionMatrix;
        portalCamera.transform.position = portalExit.transform.position;
        portalCamera.transform.rotation = cam.transform.rotation;

        CloneCameraSettings(cam, portalCamera);

        // Cull portal layer
        portalCamera.cullingMask = ~(1 << 8) & m_ReflectLayers.value;

        portalCamera.targetTexture = _portalTex;
        portalCamera.Render();

        Material[] materials = rend.sharedMaterials;

        foreach ( Material mat in materials )
        {
			if ( mat.HasProperty("_RenderTex") )
				mat.SetTexture( "_RenderTex", _portalTex );
		}
 
		// Restore pixel light count
		if( m_DisablePixelLights )
			QualitySettings.pixelLightCount = oldPixelLightCount;
 
		s_InsideRendering = false;
	}
 
 
	// Cleanup all the objects we possibly have created
	void OnDisable()
	{
		if( _portalTex ) {
            DestroyImmediate(_portalTex);
            _portalTex = null;
        }

        foreach ( DictionaryEntry kvp in m_ReflectionCameras )
			DestroyImmediate( ((Camera)kvp.Value).gameObject );

		m_ReflectionCameras.Clear();
	}
 
 
	private void CloneCameraSettings( Camera src, Camera dest )
	{
		if( dest == null )
			return;

        dest.clearFlags       = CameraClearFlags.Skybox;
        dest.backgroundColor  = src.backgroundColor;
        dest.farClipPlane     = src.farClipPlane;
        dest.nearClipPlane    = portalNearClip;
		dest.orthographic     = src.orthographic;
		dest.fieldOfView      = src.fieldOfView;
		dest.aspect           = src.aspect;
		dest.orthographicSize = src.orthographicSize;
	}
 
	// On-demand create any objects we need
	private void CreateMirrorObjects( Camera currentCamera, out Camera _portalCam )
	{

		_portalCam = null;

        // Render texture
        if (!_portalTex ) //|| m_OldReflectionTextureSize != m_TextureSize)
        {
            if (_portalTex)
                DestroyImmediate(_portalTex);

            _portalTex = new RenderTexture(1024, 1024, 16);
            _portalTex.name = "__PortalTexture" + GetInstanceID();
            _portalTex.isPowerOfTwo = true;
            _portalTex.hideFlags = HideFlags.DontSave;
        }

        // Camera for reflection
        _portalCam = m_ReflectionCameras[currentCamera] as Camera;

		if( !_portalCam ) // catch both not-in-dictionary and in-dictionary-but-deleted-GO
		{
			GameObject go = new GameObject( "Portal Camera ID " + GetInstanceID() + " for " + currentCamera.GetInstanceID(), typeof(Camera), typeof(Skybox) );
			_portalCam = go.GetComponent<Camera>();
			_portalCam.enabled = false;
			_portalCam.transform.position = trackedPos;
			_portalCam.transform.rotation = transform.rotation;
			_portalCam.gameObject.AddComponent<FlareLayer>();
            go.hideFlags = HideFlags.HideAndDontSave;
            m_ReflectionCameras[currentCamera] = _portalCam;
		}  
        
              
	}

    // Always touches the ground
    void touchGround(Transform player)
    {
        Vector3 pos = player.position;

        RaycastHit gHit;
        Ray gRay = new Ray(pos, Vector3.down);

        if (Physics.Raycast(gRay, out gHit))
        {
            pos.y = gHit.point.y + player.localScale.y; // +1 for capsule offset
            player.position = pos;
        }
    }
}